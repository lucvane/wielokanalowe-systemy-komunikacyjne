const express = require('express');
const app = express();
const Dialer = require('dialer').Dialer;
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const io = require('socket.io')(http);

const config = {
 url: 'https://uni-call.fcc-online.pl',
 login: 'focus05',
 password: '7654yhgfhn'
};

//http://focus-webapps.pl/

Dialer.configure(config);
app.use(cors());
app.use(bodyParser.json());

http.listen(3000, () => {
    console.log('app listening on port 3000');
   });
// https://amritb.github.io/socketio-client-tool/
io.on('connection', (socket) => {
    console.log('a user connected')
    socket.on('disconnect', () => {
     console.log('a user disconnected')
    })
    socket.on('message', (message) => {
            console.log('message', message)
        })
        socket.on('status', (status) => {
            console.log('status', status)
        })
        io.emit('message', 'connected!')      
   })
   
/*
app.listen(3000, () => {
 console.log('app listening on port 3000');
});
*/
let bridge = null;
/*
app.get('/call/:number1/:number2', async (req, res) => {
 //dzwonienie wykonuje się: Dialer.call(<number1>,<number2>)
 const number1 = req.params.number1;
 const number2 = req.params.number2;

 bridge = await Dialer.call(number1, number2);
 res.json({success: true});
})
*/
let currentStatus = null;

app.post('/call/', async (req, res) => {
    const body = req.body;
    console.log('body',body);
    bridge = await Dialer.call(body.number1, body.number2);
    let interval = setInterval(async () => {
        let status = await bridge.getStatus();
        if(currentStatus != status){
            io.emit("status",status)
            console.log(status)
            currentStatus = status;
        }
    },500)
     res.json({ success: true });
    })

/*
https://restninja.io/
W prawym górnym rogu ustawiamy “ajax”
• Ustawiamy metodę na POST
• Request uri: http://localhost:3000/call
• Ustawiamy body na json
{
 "number1": "xxx",
 "number2": "xxx2"
}
• Send
*/

app.get('/status', async (req, res) => {
    if(bridge != null){
        let status = await bridge.getStatus(); 
        res.json({success: true, status: status});      
    }else{
        res.json({success: true, status: "not connected"});
    }   
});

/*TODO:
W momencie inicjacji połączenie, id, na /status id jako parametr i zwracda status danego polaczenia
w gicie, wiecej niz jeden commit, instrukcja uruchomienia.
*/